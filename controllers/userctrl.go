package controllers

import (
	"fmt"
	"goappuser"
	"goappuser/database"
	"goappuser/middlewares"
	"goappuser/user"
	"log"
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

//LoginController manage login logout
type UserController struct {
	DomainBase   string
	db           dbm.DatabaseQuerier
	userManager  user.Manager
	sessionStore sessions.Store
}

//BasePath base path used for the routes of the controller
func (l *UserController) BasePath() string {
	return "/api/user"
}

//GetName Name of the controller
func (l *UserController) GetName() string {
	return "UserController"
}

//LoadController Middleware of the controller
func (l *UserController) LoadController(r *mux.Router, db dbm.DatabaseQuerier, userManager user.Manager, sess sessions.Store) {
	l.db = db
	l.userManager = userManager
	l.sessionStore = sess
	sub := r.PathPrefix(l.BasePath()).Subrouter()
	midleList := middlewares.NewMiddlewares(middlewares.NewAuthorizationMiddleware("user"))
	sub.Handle("/login", middlewares.NewMiddlewaresFunc(l.Login)).Methods("POST")
	sub.Handle("/logout", middlewares.NewMiddlewaresFunc(l.Logout)).Methods("GET")
	sub.Handle("/", midleList.UseFunc(l.User)).Methods("GET")
	sub.Handle("/", middlewares.NewMiddlewaresFunc(l.Register)).Methods("POST")
}

//Register a new user
func (l *UserController) Register(w http.ResponseWriter, r *http.Request, next func()) {

	email := r.FormValue("email")
	if len(email) <= 0 {
		app.JSONResp(w, app.RequestError{"Register", "Email missing", 0})
		next()
		return
	}
	password := r.FormValue("password")
	if len(password) <= 0 {
		app.JSONResp(w, app.RequestError{"Register", "Password missing", 1})
		next()
		return
	}
	log.Println("email ", email)
	user := user.NewUser(email, password)
	if err := l.userManager.Register(user); err != nil {
		app.JSONResp(w, app.RequestError{"Register", err.Error(), 2})
	} else {
		app.JSONResp(w, user)
	}
	next()
	return
}

//User Access to the User data in session
func (l *UserController) User(w http.ResponseWriter, r *http.Request, next func()) {
	session := context.Get(r, "Session").(*sessions.Session)
	if usr, ok := session.Values["user"].(*user.User); ok == true {
		app.JSONResp(w, usr)
	} else {
		log.Println("acced to User request in userctrl but user has no sesssion !!!")
		fmt.Println(w, http.StatusForbidden)
	}
}

//Login log in
func (l *UserController) Login(w http.ResponseWriter, r *http.Request, next func()) {
	if usr, err := l.userManager.Authenticate(r); err != nil {
		app.JSONResp(w, app.RequestError{Title: "login error", Description: err.Error(), Code: 0})
	} else {
		session, errStore := l.sessionStore.Get(r, "session-mariage")
		if errStore != nil {
			log.Println("error retrieve store session ", errStore)
			app.JSONResp(w, app.RequestError{Title: "session error", Description: err.Error(), Code: 0})
			return
		}
		log.Println("before save retrieve session ", session)

		session.Values["user"] = usr
		session.Values["user_mail"] = usr.Email
		if err := session.Save(r, w); err != nil {
			log.Println(err)
		}
		log.Println("saved session : ", session)

		app.JSONResp(w, usr)
	}
}

//Logout log out
func (l *UserController) Logout(w http.ResponseWriter, r *http.Request, next func()) {

}
